from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.contrib.hooks.ssh_hook import SSHHook

def error_log(context):
    t4_bash = """
    echo 'Logging failure'
    hostname
    whoami
    curl -i -XPOST 'http://localhost:8086/write?db=airflowtasks' --data-binary "last_task_success,host=buteo,task=grava value=0 $(date +%s)000000000"
    """
    t4 = SSHOperator(
        ssh_hook=sshHook,
        task_id='log_failure',
        command=t4_bash,
        trigger_rule='one_failed',
        dag=dag)
    t4.execute(context=context)

default_args = {
    'owner': 'michael',
    'depends_on_past': False,
    'email': ['me@mfrost.co.uk'],
    'email_on_failure': True,
    'email_on_retry': False,
    'start_date': datetime(2020, 10, 25, 21, 00),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}
dag = DAG(dag_id='grava',
          default_args=default_args,
          schedule_interval='@hourly',
          on_failure_callback=error_log,
          dagrun_timeout=timedelta(seconds=120))

sshHook = SSHHook(ssh_conn_id='ssh-michael-buteo')

t1_bash = """
echo 'Starting Grava token refresh'
hostname
whoami
docker run --rm -e GRAVA_INFLUX_HOST=influx --net=htpc -v /data/grava:/grava/authfiles --entrypoint ./token_helper.py pingue/grava-backend
"""
t1 = SSHOperator(
    ssh_hook=sshHook,
    task_id='tokenrefresh',
    command=t1_bash,
    dag=dag)

t2_bash = """
echo 'Starting Grava import'
hostname
whoami
docker run --rm -e GRAVA_INFLUX_HOST=influx --net=htpc -v /data/grava:/grava/authfiles pingue/grava-backend
"""
t2 = SSHOperator(
    ssh_hook=sshHook,
    task_id='gravaimport',
    command=t2_bash,
    dag=dag)

t3_bash = """
echo 'Logging success'
hostname
whoami
curl -i -XPOST 'http://localhost:8086/write?db=airflowtasks' --data-binary "last_task_success,host=buteo,task=grava value=1 $(date +%s)000000000"
"""
t3 = SSHOperator(
    ssh_hook=sshHook,
    task_id='log_success',
    command=t3_bash,
    trigger_rule='all_success',
    dag=dag)


t2.set_upstream(t1)
t3.set_upstream(t1)
t3.set_upstream(t2)
#t4.set_upstream(t1)
#t4.set_upstream(t2)
